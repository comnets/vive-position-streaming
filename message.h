/*
 * message.h
 *
 *  Created on: Aug 15, 2018
 *      Author: tsokalo
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <array>
#include <sstream>
#include <string.h>

#include "header.h"

struct VRButton {

	VRButton()
	{
		bt = UNDEF_BUTTON;
		st = UNDEF_BUTTON_STATE;
	}
	VRButton(ControllerButtons bt, ButtonState st)
	{
		this->bt = bt;
		this->st = st;
	}

	ControllerButtons bt;
	ButtonState st;
};

struct Message {
	static std::string convert_to_string(std::string devName, pose_t pose, std::vector<VRButton> buttons) {
		std::stringstream ss;
		ss << devName << "\t";
		for (auto v : pose)
			ss << v << "\t";
		ss << buttons.size() << "\t";
		for (auto button : buttons)
			ss << button.bt << "\t" << button.st << "\t";
		return ss.str();
	}

	static std::string convert_to_string(std::string devName, pose_t pose, std::vector<VRButton> buttons, uint16_t eventType, uint16_t buttonType) {
		std::stringstream ss;
		ss << devName << "\t";
		for (auto v : pose)
			ss << v << "\t";
		ss << buttons.size() << "\t";
		for (auto button : buttons)
			ss << button.bt << "\t" << button.st << "\t";
		ss << eventType << "\t" << buttonType << "\t";
		return ss.str();
	}

	static void convert_from_string(std::string str, pose_t &pose, std::string &devName, std::vector<VRButton> &buttons) {
		std::stringstream ss(str);
		ss >> devName;
		for (auto &v : pose)
			ss >> v;
		uint16_t s = 0;
		ss >> s;
		for(uint16_t i = 0; i < s; i++)
		{
			VRButton b;
			uint16_t x = 0;
			ss >> x;
			b.bt = ControllerButtons(x);
			ss >> x;
			b.st = ButtonState(x);
			buttons.push_back(b);
		}
	}
	static void convert_from_string(std::string str, pose_t &pose, std::string &devName, std::vector<VRButton> &buttons, uint16_t &eventType, uint16_t &buttonType) {
			std::stringstream ss(str);
			ss >> devName;
			for (auto &v : pose)
				ss >> v;
			uint16_t s = 0;
			ss >> s;
			for(uint16_t i = 0; i < s; i++)
			{
				VRButton b;
				uint16_t x = 0;
				ss >> x;
				b.bt = ControllerButtons(x);
				ss >> x;
				b.st = ButtonState(x);
				buttons.push_back(b);
			}
			ss >> eventType >> buttonType;
		}
};

//
//int main(int argc, char** argv) {
//
//	assert(argc > 1);
//
//	pose_t pose;
//	pose.fill(10);
//	auto str  = Message::convert_to_string(pose);
//	auto pose_f = Message::convert_to_pose(str);
//	for(auto v : pose_f) std::cout << v << ", ";
//	std::cout << std::endl;
//	return 0;
//
//}

#endif /* MESSAGE_H_ */
