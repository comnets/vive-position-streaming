/*
 * utils.h
 *
 *  Created on: Sep 5, 2018
 *      Author: tsokalo
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <cmath>
#include <boost/qvm/all.hpp>

#include "header.h"
#include "custom-control-comparison-utils.h"

rpj_t get_left_side_limit() {
	rpj_t min_v = { 0.3000, -0.2059, 0.2295, -1.545, -0.9497, -1.169 };
	return min_v;
}
rpj_t get_right_side_limit() {
	rpj_t max_v = { 0.5763, 0.209, 0.5523, 1.545, 0.2722, 1.143 };
	return max_v;
}

rpj_t get_initial_arm_rpj() {
	rpj_t v = { 0.3069, 5.103e-07, 0.4869, 3.142, -5.834e-07, 4.158e-06 };
	return v;
}

// http://planning.cs.uiuc.edu/node102.html
pose_t translate_cartesian_to_homogeneous(rpj_t cartesian_coord) {

	pose_t m;

	auto s = [](double a)->double {return sin(a);};
	auto c = [](double a)->double {return cos(a);};

	double x = cartesian_coord[0];
	double y = cartesian_coord[1];
	double z = cartesian_coord[2];
	double r = cartesian_coord[3];
	double p = cartesian_coord[4];
	double j = cartesian_coord[5];

	//
	// rotation
	//
	m[0] = (c(j) * c(p));
	m[1] = (s(j) * c(p));
	m[2] = (-s(p));
	m[3] = 0;

	m[4] = (c(j) * s(p) * s(r) - s(j) * c(r));
	m[5] = (s(j) * s(p) * s(r) + c(j) * c(r));
	m[6] = (c(p) * s(r));
	m[7] = 0;

	m[8] = (c(j) * s(p) * c(r) + s(j) * s(r));
	m[9] = (s(j) * s(p) * c(r) - c(j) * s(r));
	m[10] = (c(p) * c(r));
	m[11] = 0;

	//
	// displacement
	//
	m[12] = x;
	m[13] = y;
	m[14] = z;
	m[15] = 1;

	return m;
}

pose_t multiply(pose_t to_trans, pose_t trans_by) {
	uint16_t s = 4;
	pose_t mult;
	for (auto &v : mult)
		v = 0;
	for (uint16_t i = 0; i < s; i++)
		for (uint16_t j = 0; j < s; j++)
			for (uint16_t k = 0; k < s; k++) {
				mult[4 * i + j] += to_trans[4 * i + k] * trans_by[4 * k + j];
			}
	return mult;
}

rpj_t translate_honogeneous_to_cartesian(pose_t m) {

	rpj_t cart;

	auto at = [](double a, double b)->double {
		return atan2(a, b);
	};

	cart[0] = m[12]; 										// x
	cart[1] = m[13]; 										// y
	cart[2] = m[14]; 										// z
	cart[3] = at(m[6], m[10]);								// rotation around x
	cart[4] = at(-m[2], sqrt(m[6] * m[6] + m[10] * m[10]));	// rotation around y
	cart[5] = at(m[1], m[0]);								// rotation around z

	return cart;
}

struct RPJToScreen: public rpj_t {
	RPJToScreen(rpj_t &rpj) {
		std::copy(std::begin(rpj), std::end(rpj), std::begin(*this));

	}
	friend std::ostream& operator<<(std::ostream& os, const RPJToScreen& obj) {
		os << "[";
		for (auto v : obj)
			os << std::setw(10) << v << ",";
		os << "]";
		return os;
	}
};

struct PoseToMatrix: public pose_t {
	PoseToMatrix(pose_t &pose) {
		std::copy(std::begin(pose), std::end(pose), std::begin(*this));

	}
	friend std::ostream& operator<<(std::ostream& os, const PoseToMatrix& obj) {

		for (uint16_t i = 0; i < 4; i++) {
			for (uint16_t j = 0; j < 4; j++) {
				os << std::setw(15) << obj.at(j * 4 + i) << "\t";
			}
			os << std::endl;
		}

		return os;
	}
};
struct RPJToFile: public rpj_t {
	RPJToFile(rpj_t &rpj) {
		std::copy(std::begin(rpj), std::end(rpj), std::begin(*this));

	}
	friend std::ostream& operator<<(std::ostream& os, const RPJToFile& obj) {
		for (auto v : obj)
			os << v << "\t";
		return os;
	}
};

struct PoseToScreen: public pose_t {
	PoseToScreen(pose_t &pose) {
		std::copy(std::begin(pose), std::end(pose), std::begin(*this));

	}
	friend std::ostream& operator<<(std::ostream& os, const PoseToScreen& obj) {
		os << "[";
		for (auto v : obj)
			os << v << ",";
		os << "]";
		return os;
	}
};

struct PoseToFile: public pose_t {
	PoseToFile(pose_t &pose) {
		std::copy(std::begin(pose), std::end(pose), std::begin(*this));

	}
	friend std::ostream& operator<<(std::ostream& os, const PoseToFile& obj) {
		for (auto v : obj)
			os << v << "\t";
		return os;
	}
};

pose_t adapt_htc_to_franka_matrix(pose_t h) {
	pose_t f;

	for (uint16_t i = 0; i < 4; i++)
		for (uint16_t j = 0; j < 4; j++)
			f[i * 4 + j] = h[j * 4 + i];

	return f;
}

pose_t invert(pose_t L) {
	pose_t M;
	uint16_t n = 4;

	int16_t i, j, k;
	double h;

	//create unity matrix
	for (j = 0; j < n; j++)
		for (i = 0; i < n; i++)
			M[i + j * n] = i != j ? 0 : 1;

	//for each row of the matrix
	for (j = 0; j < n; j++) {
		k = 0;
		if (!L[j * n + j])    //wenn das zu normierende element 0 ist zeilentausch
			for (k = j + 1; k < n; ++k)    //für jede zeile nach der jten
				if (L[k * n + j]) {    //wenn element ungleich 0 gefunden zeilen tauschen und schleife abbrechen
					for (i = 0; i < n; ++i) {
						h = L[j * n + i];
						L[j * n + i] = L[k * n + i];
						L[k * n + i] = h;
						h = M[j * n + i];
						M[j * n + i] = M[k * n + i];
						M[k * n + i] = h;
					}
					break;
				}
		if (k == n) {
			assert(0);
		}

		h = L[j * n + j];    //wert des zu normierenden elements
		for (k = 0; k < n; k++) {
			assert(!eq(h,0));
			L[k + j * n] /= h;
			M[k + j * n] /= h;
		}    //die komplette zeile normieren
		for (i = 0; i < n; i++)    // gauss über die matrix jagen
			if (i != j && L[i * n + j] != 0) {
				h = -L[i * n + j];
				for (k = 0; k < n; k++) {
					L[k + i * n] += h * L[k + j * n];
					M[k + i * n] += h * M[k + j * n];
				}
			}

	}
	return M;
}

void adapt_to_franka_rpj(rpj_t &h) {

//	std::swap(h[CC::x], h[CC::z]);
//	h[CC::y] = h[CC::y];
//	h[CC::roll] = h[CC::roll];
//	std::swap(h[CC::roll], h[CC::pitch]);
}


template <typename T, int Rows, int Cols>
std::ostream& operator<<(std::ostream& os, const boost::qvm::mat<T, Rows, Cols>& m)
{
    os << "[ ";
    for (int r = 0; r < Rows - 1; ++r)
    {
        for (int c = 0; c < Cols - 1; ++c) os << m.a[r][c] << ", ";
        if (Cols > 0) os << m.a[r][Cols - 1];
        os << "\n  ";
    }
    if (Rows > 0)
    {
        for (int c = 0; c < Cols - 1; ++c) os << m.a[Rows - 1][c] << ", ";
        if (Cols > 0) os << m.a[Rows - 1][Cols - 1];
    }
    os << " ]";
    return os;
}


struct CalibrationData {
	friend std::ostream& operator<<(std::ostream& os, CalibrationData& obj) {

		os << "trans: " << obj.trans << ", controller origin: " << RPJToScreen(obj.controllerOrigin) << ", robot origin: "
				<< RPJToScreen(obj.robotOrigin);
		return os;
	}
	// scaling and rotation
	boost::qvm::mat<double,4,4> trans;
	// origin of the HTC controller
	rpj_t controllerOrigin;
	// origin of the robot
	rpj_t robotOrigin;
};



#endif /* UTILS_H_ */
