/*
 * header.h
 *
 *  Created on: Sep 5, 2018
 *      Author: tsokalo
 */

#ifndef HEADER_H_
#define HEADER_H_

#include <array>
#include <iostream>
#include <sstream>
#include <fstream>

enum Mode {
	COMM_MODE, EVAL_MODE, TEST_MODE
};

typedef std::array<double, 16> pose_t;
typedef std::array<double, 6> rpj_t;
enum ControllerButtons {
	GRIP_BUTTON, TRIGGER_BUTTON, TOUCHPAD_BUTTON, MENU_BUTTON, UNDEF_BUTTON
};

enum ButtonState {
	PRESS_BUTTON, UNPRESS_BUTTON, TOUCHPAD_UP_BUTTON, TOUCHPAD_DOWN_BUTTON, TOUCHPAD_RIGHT_BUTTON, TOUCHPAD_LEFT_BUTTON, UNDEF_BUTTON_STATE
};
struct CC {
	enum CartesianCoordinate {
		x, y, z, roll, pitch, yaw
	};
};



#endif /* HEADER_H_ */
