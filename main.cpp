// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <cmath>
#include <iostream>
#include <assert.h>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include "udp-server.h"
#include "udp-client.h"
#include "message.h"
#include "header.h"
#include "utils.h"
#include "controller-interface.h"

int main(int argc, char** argv) {

	assert(argc > 1);

	Mode m = Mode(atoi(argv[1]));

	if (m == EVAL_MODE) {

//		std::string file_name = "htc_points.txt";
//		std::ifstream infile(file_name, std::ifstream::in);
//		std::vector<pose_t> poses;
//
//		std::string line;
//		while (std::getline(infile, line)) {
//			std::istringstream iss(line);
//			poses.push_back(Message::convert_to_pose(line));
//		}
//		infile.close();
//
//		uint32_t c = 0;
//		for (auto pose : poses) {
//			pose = adapt_htc_to_franka_matrix(pose);
////			std::cout << PoseToScreen(pose) << std::endl;
//			auto rpj = translate_honogeneous_to_cartesian(pose);
//
//			std::cout << c++ << "\t" << RPJToFile(rpj) << std::endl;
//		}

	} else if (m == COMM_MODE) {

		assert(argc > 2);

		uint16_t port = 6000;
		std::string host = "127.0.0.1";
		std::string devMatchName = "Hand";

		auto time_last = std::chrono::high_resolution_clock::now();

		if (std::string(argv[2]) == "client") {
			boost::asio::io_service io_service;
			UdpClient client(io_service, host, std::to_string(port));

			pose_t pose;
			pose.fill(10);
			std::vector<VRButton> buttons = { VRButton(GRIP_BUTTON, PRESS_BUTTON), VRButton(TRIGGER_BUTTON, UNPRESS_BUTTON), VRButton(TOUCHPAD_BUTTON,
					TOUCHPAD_DOWN_BUTTON), VRButton(MENU_BUTTON, UNPRESS_BUTTON) };
			std::string devName = "RightHand";

			client.send(Message::convert_to_string(devName, pose, buttons));

		} else if (std::string(argv[2]) == "server") {
			try {

				std::cout << std::setprecision(2);
				std::cout << "Running server" << std::endl;

				callback_up forward_up = [&time_last, &devMatchName](std::string msg) {

					pose_t pose;
					std::string devName;
					std::vector<VRButton> buttons;
					uint16_t eventType, buttonType;
					Message::convert_from_string(msg, pose, devName, buttons, eventType, buttonType);
					if(devName.find(devMatchName) == std::string::npos)return;
					auto time_now = std::chrono::high_resolution_clock::now();
//				std::cout << std::chrono::duration_cast<std::chrono::microseconds>(time_now - time_last).count() << " ";
						time_last = time_now;

						std::cout << devMatchName << ": ";

						std::cout << PoseToFile(pose);
						std::cout << " | ";
						for(auto button : buttons)
						{
							std::cout << "<" << button.bt << "," << button.st << ">";
						}
						std::cout << " | ";
						std::cout << eventType << "," << buttonType;
						std::cout << std::endl;
					};
				boost::asio::io_service io_service;
				UdpServer server(io_service, port, forward_up);
				io_service.run();

			} catch (std::exception& e) {
				std::cerr << e.what() << std::endl;
			}
		} else {
			assert(0);
		}
	} else if (m == TEST_MODE) {

		std::cout << std::setprecision(4);

		auto commandRobot = [](CommandToRobot command) {
			std::cout << "Received command " << command.command << " rpj " << RPJToScreen(command.rpj) << std::endl;
		};
		ControllerInterface commIface(commandRobot);

		while(1)
		{

		}
	}

	return 0;
}
