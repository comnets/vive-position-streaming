/*
 * udp-server.h
 *
 *  Created on: Aug 15, 2018
 *      Author: tsokalo
 */

#ifndef SAMPLES_HELLOVR_OPENGL_UDP_SERVER_H_
#define SAMPLES_HELLOVR_OPENGL_UDP_SERVER_H_

//
// server.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2008 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <ctime>
#include <iostream>
#include <string>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::udp;

typedef std::function<void(std::string)> callback_up;

class UdpServer {
public:
	UdpServer(boost::asio::io_service& io_service, uint16_t port, callback_up forward_up) :
			socket_(io_service, udp::endpoint(udp::v4(), port)), forward_up_(forward_up) {
		start_receive();
	}

private:
	void start_receive() {
		socket_.async_receive_from(boost::asio::buffer(recv_buffer_), remote_endpoint_,
				boost::bind(&UdpServer::handle_receive, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	}

	void handle_receive(const boost::system::error_code& error, std::size_t bytes) {
		if (!error) {
			forward_up_(std::string(recv_buffer_.begin(), recv_buffer_.end()));
			start_receive();
		}
	}

	udp::socket socket_;
	udp::endpoint remote_endpoint_;
	boost::array<char, 1024> recv_buffer_;
	callback_up forward_up_;
};

//int main() {
//	try {
//		callback_up forward_up = [](std::string msg) {std::cout << "Received " << msg << std::endl;};
//		boost::asio::io_service io_service;
//		UdpServer server(io_service, 6000, forward_up);
//		io_service.run();
//	} catch (std::exception& e) {
//		std::cerr << e.what() << std::endl;
//	}
//
//	return 0;
//}

#endif /* SAMPLES_HELLOVR_OPENGL_UDP_SERVER_H_ */
