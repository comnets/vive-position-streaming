/*
 * comparison.h
 *
 *  Created on: 21.10.2016
 *      Author: tsokalo
 */

#ifndef COMPARISON_H_
#define COMPARISON_H_

#define PRECISION_	10e-8
#include <cmath>
#include <limits>

inline bool leq(double lhs, double rhs) {
	return (lhs - PRECISION_ < rhs);
}
inline bool geq(double lhs, double rhs) {
	return (lhs + PRECISION_ > rhs);
}
inline bool eq(double lhs, double rhs) {
	return (std::abs(lhs - rhs) < PRECISION_);
}
inline bool gr(double lhs, double rhs) {
	return (!leq(lhs, rhs));
}
inline bool le(double lhs, double rhs) {
	return (!geq(lhs, rhs));
}
inline bool neq(double lhs, double rhs) {
	return !eq(lhs, rhs);
}
inline bool eqzero(double lhs) {
	return eq(lhs, 0);
}


struct comp_signed {
	comp_signed(double v) {
		v_ = v;
	}
	friend inline bool operator<(const comp_signed& lhs, const comp_signed& rhs) {
		if (lhs.v_ < 0 && rhs.v_ > 0) return true;
		if (lhs.v_ > 0 && rhs.v_ < 0) return false;
		return le(fabs(lhs.v_), fabs(rhs.v_));
	}
	friend inline bool operator>(const comp_signed& lhs, const comp_signed& rhs) {
		if (lhs.v_ < 0 && rhs.v_ > 0) return false;
		if (lhs.v_ > 0 && rhs.v_ < 0) return true;
		return gr(fabs(lhs.v_), fabs(rhs.v_));
	}
	friend inline bool operator<=(const comp_signed& lhs, const comp_signed& rhs) {
		return !(lhs > rhs);
	}
	friend inline bool operator>=(const comp_signed& lhs, const comp_signed& rhs) {
		return !(lhs < rhs);
	}
	double v_;
};

#endif /* COMPARISON_H_ */
