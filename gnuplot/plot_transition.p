set terminal svg enhanced size 1200 900 font "Times New Roman, 22" solid
set encoding iso_8859_1

set border 4095 front linetype -1 linewidth 1.000
set ticslevel 0


set boxwidth 0.6 relative
set style fill solid 1.0
set grid xtics ytics back lw 1 lc rgb "#AFAFAF"
set key right top
set output 'transition1.svg'

PI=3.14159
pi(x)=PI / 2 *x
angle(x)=PI/2 * (1 - cos(pi(x)))
f1(x)=sin(angle(x*0.55))
f2(x)=sin(angle(angle(x*0.55)))
f3(x)=sin(angle(angle(angle(x*0.55))))

w1(x)=f1(x)/f1(1)
w2(x)=f2(x)/f2(1)
w3(x)=f3(x)/f3(1)

set samples 800
plot [0:1] w1(x) title "1", w2(x) title "2", w3(x) title "3"

set output 'transition.svg'
uerfc(x,a)=(erfc(-(x - 0.5)*a*2) / 2)
scaleerr(a)=(uerfc(0,a)+(1-uerfc(1,a)))
uaerfc(x,a)=uerfc(x,a)*(1+scaleerr(a)) - scaleerr(a)/2

plot [0:1] f1(x), uaerfc(x,5), uaerfc(x,4), uaerfc(x,3), uaerfc(x,2.3), uaerfc(x,2.0) 

