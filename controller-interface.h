/*
 * controller-interface.h
 *
 *  Created on: Sep 5, 2018
 *      Author: tsokalo
 */

#ifndef CONTROLLER_INTERFACE_H_
#define CONTROLLER_INTERFACE_H_

#include <cmath>
#include <iostream>
#include <assert.h>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <iostream>
#include <vector>
#include <fstream>
#include <thread>
#include <string>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/qvm/mat_operations.hpp>
#include <boost/qvm/mat.hpp>
#include <boost/qvm/mat_access.hpp>

#include "udp-server.h"
#include "header.h"
#include "message.h"
#include "utils.h"
#include "custom-control-comparison-utils.h"

#define ENABLE_CONTROLLER_INTERFACE_LOG	0

struct CommandToRobot {

	enum Command {
		STOP, MOVE_TO_POINT, GO_TO_INITIAL_POSITION, NOT_INITIALIZED
	};

	CommandToRobot() {
		command = NOT_INITIALIZED;
		rpj.fill(0);
	}
	CommandToRobot(Command command, rpj_t rpj) {
		this->command = command;
		this->rpj = rpj;
	}
	CommandToRobot(Command command) {
		this->command = command;
		rpj.fill(0);
	}

	Command command;
	rpj_t rpj;
};

class ControllerInterface {

	typedef std::function<void(CommandToRobot)> command_func;

	enum State {
		NEED_CALIBRATION_POINT, CALIBRATED
	};
public:

	ControllerInterface(command_func func) :
			m_sendCommand(func), m_commPort(6000), m_server(io_service_, m_commPort,
					std::bind(&ControllerInterface::ProcessMsg, this, std::placeholders::_1)) {

		m_state = NEED_CALIBRATION_POINT;
		m_numCPoints = 4;
		std::thread t([this] () {this->io_service_.run();});
		m_rcvThread.swap(t);

		m_lastButtons.push_back(VRButton(GRIP_BUTTON, UNDEF_BUTTON_STATE));
		m_lastButtons.push_back(VRButton(TRIGGER_BUTTON, UNDEF_BUTTON_STATE));
		m_lastButtons.push_back(VRButton(TOUCHPAD_BUTTON, UNDEF_BUTTON_STATE));
		m_lastButtons.push_back(VRButton(MENU_BUTTON, UNDEF_BUTTON_STATE));

		m_lastSendCommandTP = std::chrono::system_clock::now();

		std::cout << "Controller interface started" << std::endl;
	}
	~ControllerInterface() {
		io_service_.stop();
		m_rcvThread.join();
	}

private:

	void ProcessMsg(std::string msg) {

		std::string devMatchName = "Hand";
		pose_t pose;
		std::string devName;
		std::vector<VRButton> buttons;
		uint16_t eventType, buttonType;
		Message::convert_from_string(msg, pose, devName, buttons, eventType, buttonType);
		pose = adapt_htc_to_franka_matrix(pose);

		if (devName.find(devMatchName) == std::string::npos)
			return;

		ProcessButtons(buttons, pose);
		ProcessPose(pose);

		if (ENABLE_CONTROLLER_INTERFACE_LOG) {

			auto newPoint = translate_honogeneous_to_cartesian(pose);
			adapt_to_franka_rpj(newPoint);
			std::cout << devMatchName << ": ";
			std::cout << RPJToScreen(newPoint);
			std::cout << " | ";
			for (auto button : buttons) {
				std::cout << "<" << button.bt << "," << button.st << ">";
			}
			std::cout << " | ";
			std::cout << eventType << "," << buttonType;
			std::cout << std::endl;
		}
	}

	void ProcessButtons(std::vector<VRButton> buttons, pose_t pose) {
		//
		// check if any new button event happened
		//
		assert(m_lastButtons.size() == buttons.size());
		auto n = m_lastButtons.size();
		bool smth_new = false;
		for (uint16_t i = 0; i < n; i++) {
			if (buttons[i].st != m_lastButtons[i].st) {
				smth_new = true;
				break;
			}
		}
		m_lastButtons = buttons;

		//
		// process the button event
		//
		if (smth_new) {
			switch (m_state) {
			case NEED_CALIBRATION_POINT: {
				if (buttons[TRIGGER_BUTTON].st == PRESS_BUTTON) {
					auto rpj = translate_honogeneous_to_cartesian(pose);
					adapt_to_franka_rpj(rpj);
					m_cPoints.push_back(rpj);

					std::cout << "Received first calibration point " << RPJToScreen(rpj) << std::endl;
					if (m_cPoints.size() == m_numCPoints) {
						m_state = Calibrate() ? CALIBRATED : NEED_CALIBRATION_POINT;
						if (m_state != CALIBRATED) {
							std::cout << "Calibration failed. Start anew" << std::endl;
							m_cPoints.clear();
						}
					}
				}
				break;
			}
			case CALIBRATED: {
				if (buttons[MENU_BUTTON].st == PRESS_BUTTON) {
					m_sendCommand(CommandToRobot(CommandToRobot::STOP));
					SetNeedCalibration();
				}
				break;
			}
			}
		}
	}
	void ProcessPose(pose_t pose) {
		if (m_state == CALIBRATED) {
			auto rc = TranslateMovement(pose);

			auto time_now = std::chrono::system_clock::now();
			auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_now - m_lastSendCommandTP);
			if (duration >= std::chrono::milliseconds(1000)) {
				m_lastSendCommandTP = time_now;
				std::cout << " Commanded robot location: " << RPJToScreen(rc) << std::endl;
				m_sendCommand(CommandToRobot(CommandToRobot::MOVE_TO_POINT, rc));
			}

		} else {
			// do nothing
		}
	}

	void SetNeedCalibration() {
		m_state = NEED_CALIBRATION_POINT;
		m_cPoints.clear();
	}

	bool Calibrate() {

		assert(m_cPoints.size() == m_numCPoints);

		using namespace boost::qvm;

		auto ini_rpj = get_initial_arm_rpj();
		auto min_rpj = get_left_side_limit();
		auto max_rpj = get_right_side_limit();

		std::cout << "Robot space \n" << RPJToScreen(ini_rpj) << "\nController space \n" << RPJToScreen(m_cPoints[0]) << std::endl;

		mat<double, 4, 4> rspace;
		A00(rspace) = min_rpj[CC::x];
		A01(rspace) = max_rpj[CC::x];
		A02(rspace) = min_rpj[CC::x];
		A03(rspace) = min_rpj[CC::x];

		A10(rspace) = ini_rpj[CC::y];
		A11(rspace) = ini_rpj[CC::y];
		A12(rspace) = max_rpj[CC::y];
		A13(rspace) = ini_rpj[CC::y];

		A20(rspace) = ini_rpj[CC::z];
		A21(rspace) = ini_rpj[CC::z];
		A22(rspace) = ini_rpj[CC::z];
		A23(rspace) = max_rpj[CC::z];

		A30(rspace) = 1;
		A31(rspace) = 1;
		A32(rspace) = 1;
		A33(rspace) = 1;

		mat<double, 4, 4> cspace;
		auto cpose = translate_cartesian_to_homogeneous(m_cPoints[0]);
		A00(cspace) = m_cPoints[0][0];
		A01(cspace) = m_cPoints[1][0];
		A02(cspace) = m_cPoints[2][0];
		A03(cspace) = m_cPoints[3][0];

		A10(cspace) = m_cPoints[0][1];
		A11(cspace) = m_cPoints[1][1];
		A12(cspace) = m_cPoints[2][1];
		A13(cspace) = m_cPoints[3][1];

		A20(cspace) = m_cPoints[0][2];
		A21(cspace) = m_cPoints[1][2];
		A22(cspace) = m_cPoints[2][2];
		A23(cspace) = m_cPoints[3][2];

		A30(cspace) = 1;
		A31(cspace) = 1;
		A32(cspace) = 1;
		A33(cspace) = 1;

		std::cout << "Robot space \n" << rspace << "\nController space \n" << cspace << std::endl;
		m_calibration.trans = rspace * inverse(cspace);
		std::cout << "Transformation matrix \n" << m_calibration.trans << "\nShould be the Robot space \n" << (m_calibration.trans * cspace)
				<< std::endl;

		boost::qvm::vec<double, 4> input = { { m_cPoints[0][CC::x], m_cPoints[0][CC::y], m_cPoints[0][CC::z], 1 } };
		auto output = m_calibration.trans * input;
		rpj_t rc;
		rc.fill(0);
		rc[CC::x] = boost::qvm::A0(output);
		rc[CC::y] = boost::qvm::A1(output);
		rc[CC::z] = boost::qvm::A2(output);
		std::cout << "New point " << RPJToScreen(rc) << std::endl;
		//
		// TODO: check if (cspace * inv) is identity matrix
		//

		return true;
	}

	/*
	 * translating the movement of the controller to the movement of the robot
	 */
	rpj_t TranslateMovement(pose_t cpose) {

		using namespace boost::qvm;
		auto newPoint = translate_honogeneous_to_cartesian(cpose);
		adapt_to_franka_rpj(newPoint);

		boost::qvm::vec<double, 4> input = { { newPoint[CC::x], newPoint[CC::y], newPoint[CC::z], 1 } };
		auto output = m_calibration.trans * input;
		rpj_t rc = get_initial_arm_rpj();;
		rc[CC::x] = boost::qvm::A0(output);
		rc[CC::y] = boost::qvm::A1(output);
		rc[CC::z] = boost::qvm::A2(output);

		return rc;
	}

private:

	boost::asio::io_service io_service_;
	uint16_t m_commPort;
	UdpServer m_server;
	State m_state;
	command_func m_sendCommand;
	std::vector<VRButton> m_lastButtons;
	std::chrono::system_clock::time_point m_lastSendCommandTP;

	std::thread m_rcvThread;
	/*
	 * calibration points
	 * 1st - hold hand next to chest
	 * 2nd - hold hand straight in front of you
	 * 3rd - hold hand to the right of you
	 * 4th - hold hand to stretched to the top
	 */
	std::vector<rpj_t> m_cPoints;
	uint16_t m_numCPoints;
	CalibrationData m_calibration;

};

#endif /* CONTROLLER_INTERFACE_H_ */
